The app will work in the Outside School Hours Care services provide care before and after school hours and during school holidays for children who normally attend school.

We have a booking platform for parents to enrol their children with a 'Service' and to book and attend 'Sessions' at that service. Services are run and managed by a 'Provider'.

Australian parents are potentially entitled to a Child Care Subsidy (CCS) payment from the Australian government if their child attends a service at that is run by an approved Provider under Family Assistance Law. As such, providers, their services and the 'Personnel' that work at either of them are required to register with the Department of Human Services (DHS).

The DHS provides several webservices to enable the management of this process, this project considers the first service - Account Management and the presentation and manipulation of its data.

Account Management Service
The Account Management service is used to query and update data held within the Child Care Subsidy system specific to a Provider and Service profile.
The Account Management service will offer the following operations:

Read Provider Profile
Update Provider Profile
Read Service Profile
Update Service Profile
Read Personnel
Add Personnel
Update Personnel
Upload Supporting Evidence
Provider profile details include basic information on the providers (name, address, contact, financial) and additional data where applicable (trustee, holding company, other).

Service profile details include basic information on the Services (name, address, contact, financial), operational information (places offered, weeks of operation, fee information) and additional data where applicable (trustee, external management, temporary ceasing, other). A fee structure table is also able to be added and amended to assist with prepopulating enrolment fee information.

Personnel details include details on individuals associated with the Services (name, contact details) and additional data where applicable (Working with Children Card).

Required Work
The work we require to be attempted is to build a simple rails application that does, can or enables...:

Presents a search to look up an existing CCS Service (Services can only be read and updated)
Make calls to the Swagger 'Account Management Service' client api stub 'Service' operation,
Example json exists in to populate DOM (lib/swaggers/Account_Management_Service/data).
Example api code provided below#
Feel free to duplicate or recreate existing data as required
Present the Service DOM as a form
The form data must be editable for the existing data
The ability to add new service profile child object data structures e.g 'ServiceProfile.Fees' would be ideal
A small set of example field validation (2-5 fields) to be applied as per the linked api data document https://docs.google.com/document/d/1vYyVtnvUfIG4KhZ0bRrkvmiClwiChSUlBaigONq0kro/edit?usp=sharing
The form must send the data back to the server such that it can be marshalled back into the swagger client dom to be validated. A view or simple logging or notice will do
A simple CSS theme can be used if required, in production we use 'Adminto Dark Dashboard Template'
With the exception of key identifiers such as 'ccs service id', the api data is not persisted locally, the api is the data store, hence the need for a search / lookup form for known key data objects e.g. D.O.B surname for a person
Example unit test specs
Deployed to something like heroku
SQLLite for db is fine