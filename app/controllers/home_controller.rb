class HomeController < ApplicationController
  def index
  	if params[:address].present?
  		@address = Address.find_by_id(params[:address])
  	end
  	if !@address.present?
  	 @address = Address.new
  	end
  	@addresses = Address.all
  end
end
