json.extract! address, :id, :type, :street_line1, :street_line2, :suburb, :state, :postcode, :start_date, :end_date, :created_at, :updated_at
json.url address_url(address, format: :json)
