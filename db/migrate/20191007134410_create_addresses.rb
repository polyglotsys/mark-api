class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :address_type
      t.string :street_line1
      t.string :street_line2
      t.string :suburb
      t.string :state
      t.string :postcode
      t.string :start_date
      t.string :end_date
      t.string :number, default: 1

      t.timestamps
    end
  end
end
